use persy::{ByteVec, Config, Persy, PersyError, TransactionConfig, ValueMode};

use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut config = Config::new();
    config.change_cache_size(1024 * 1024 * 256);
    let persy = Persy::open_or_create_with("./loaded_data.persy", config, |_p| Ok(()))?;

    let atomic = Arc::new(AtomicUsize::new(0));
    if !persy.exists_index("test")? {
        let mut tx = persy.begin()?;

        tx.create_index::<ByteVec, ByteVec>("test", ValueMode::Replace)?;

        let prepared = tx.prepare()?;

        prepared.commit()?;
    }

    let reader = atomic.clone();
    std::thread::spawn(move || loop {
        std::thread::sleep(std::time::Duration::from_millis(1000));

        println!("{} recs/second", reader.load(Ordering::SeqCst));
        reader.store(0, Ordering::SeqCst);
    });

    let mut run = 0;
    loop {
        let mut handles: Vec<std::thread::JoinHandle<Result<(), PersyError>>> = vec![];
        for t in 0..10 {
            let db = persy.clone();
            let atomic_cloned = atomic.clone();
            handles.push(std::thread::spawn(move || {
                let mut tx = db.begin_with(TransactionConfig::new().set_background_sync(true)).map_err(|e|e.error())?;
                for n in 0..1000000 {
                    let kv = ByteVec::from(format!("{}+{}+{}", run, t, n).as_bytes());
                    tx.put("test", kv.clone(), kv).map_err(|e|e.error())?;
                    if n % 1 == 0 {
                        let prepared = tx.prepare().map_err(|e|e.error())?;
                        prepared.commit().map_err(|e|e.error())?;
                        tx = db.begin_with(TransactionConfig::new().set_background_sync(true)).map_err(|e|e.error())?;
                    }
                    atomic_cloned.fetch_add(1, Ordering::SeqCst);
                }

                Ok(())
            }))
        }

        for t in handles {
            t.join().unwrap()?;
        }
        run += 1;
        let mut input = String::new();
        std::io::stdin().read_line(&mut input)?;
        if input.starts_with("n") {
            break;
        }
    }

    Ok(())
}
